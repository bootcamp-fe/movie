module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    extend: {
      keyframes: {
        slideToRight: {
          "0%": { width: "0%" },
          "100%": { width: "60%" },
        },
      },
      animation: {
        slideToRight: "slideToRight 1s forwards",
      },
    },
  },
  plugins: [],
};
