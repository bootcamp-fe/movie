import { applyMiddleware, combineReducers, createStore, compose } from "redux";
import thunk from "redux-thunk";
import { HomepageModalReducer } from "./reducers/HomepageModalReducer";
import { QuanLyPhimReducer } from "./reducers/QuanLyPhimReducer";
import { MovieDetailReducer } from "./reducers/MovieDetailReducer";
import { SpinnerReducer } from "./reducers/SpinnerReducer";
import { UserReducer } from "./reducers/UserReducer";
import { TicketReducer } from "./reducers/TicketReducer";

const rootReducer = combineReducers({
  // state ứng dụng
  QuanLyPhimReducer,
  HomepageModalReducer,
  MovieDetailReducer,
  SpinnerReducer,
  UserReducer,
  TicketReducer,
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export const store = createStore(
  rootReducer,
  composeEnhancers(applyMiddleware(thunk))
);
