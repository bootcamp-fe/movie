import { localServ } from "~/services/localService";
import { quanLyPhimService } from "~/services/QuanLyPhimService";
import { SET_MOVIE_LIST } from "./constants/constants";

export const setMovieList = async (dispatch) => {
  try {
    let result = await quanLyPhimService.layDanhSachPhim();
    dispatch({ type: SET_MOVIE_LIST, data: result.data.content });
  } catch (err) {
    console.log(err);
  }
};
