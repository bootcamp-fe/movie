import { quanLyPhimService } from "~/services/QuanLyPhimService";
import { SET_MOVIE_INFO } from "./constants/constants";
import { setSpinnerOff, setSpinnerOn } from "./SpinnerAction";

export const getShowTimeById = (id) => {
  return async (dispatch) => {
    try {
      dispatch(setSpinnerOn());
      let result = await quanLyPhimService.layLichChieuTheoPhim(id);
      dispatch({ type: SET_MOVIE_INFO, data: result.data.content });
      // Delay off spinner a bit for prevent flickering
      setTimeout(() => {
        dispatch(setSpinnerOff());
      }, 300);
    } catch (err) {
      dispatch(setSpinnerOff());
      console.log(err);
    }
  };
};
