import { quanLyDatVeServ } from "~/services/QuanLyDatVeService";
import { SET_THEATER_INFO } from "./constants/constants";
import { setSpinnerOff, setSpinnerOn } from "./SpinnerAction";

export const getTheaterInfo = (id) => {
  return async (dispatch) => {
    try {
      dispatch(setSpinnerOn());
      let result = await quanLyDatVeServ.layThongTinRapChieu(id);
      dispatch({ type: SET_THEATER_INFO, data: result.data.content });
      dispatch(setSpinnerOff());
    } catch (err) {
      dispatch(setSpinnerOff());
      console.log(err);
    }
  };
};
