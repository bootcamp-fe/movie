import { quanLyPhimService } from "~/services/QuanLyPhimService";
import { SET_CAROUSEL } from "./constants/constants";

export const getCarouselImg = () => {
  return async (dispatch) => {
    try {
      const result = await quanLyPhimService.layDanhSachBanner();
      dispatch({
        type: SET_CAROUSEL,
        data: result.data.content,
      });
    } catch (err) {
      console.log(err);
    }
  };
};
