import { SET_LOADING } from "./constants/constants";

export const setSpinnerOn = () => {
  return { type: SET_LOADING, data: true };
};

export const setSpinnerOff = () => {
  return { type: SET_LOADING, data: false };
};
