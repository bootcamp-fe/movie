import { message } from "antd";
import { localServ } from "~/services/localService";
import { quanLyNguoiDungServ } from "~/services/QuanLyNguoiDungService";
import { SET_USER_INFO } from "./constants/constants";

export const PostLogin = (data, onLoginSuccess, onLoginFail) => {
  return async (dispatch) => {
    try {
      let result = await quanLyNguoiDungServ.dangNhap(data);
      localServ.user.set(result.data);
      onLoginSuccess();
    } catch (err) {
      onLoginFail();
    }
  };
};
