import { SET_USER_INFO } from "../actions/constants/constants";

const initialState = {
  userInfo: null,
};

export const UserReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_USER_INFO:
      return { ...state, userInfo: action.data };
    default:
      return { ...state };
  }
};
