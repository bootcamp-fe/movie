import {
  CLOSE_TRAILER_MODAL,
  OPEN_TRAILER_MODAL,
} from "../actions/constants/constants";

const initialValue = {
  isVideoModalOpen: false,
  youtubeId: "",
};

let youtube_parser = (url) => {
  let regExp =
    /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#&?]*).*/;
  let match = url.match(regExp);
  return match && match[7].length == 11 ? match[7] : false;
};

export const HomepageModalReducer = (state = initialValue, action) => {
  switch (action.type) {
    case OPEN_TRAILER_MODAL:
      let youtubeId = youtube_parser(action.data);
      return { ...state, isVideoModalOpen: true, youtubeId: youtubeId };
    case CLOSE_TRAILER_MODAL:
      return { ...state, isVideoModalOpen: false, youtubeId: "" };
    default:
      return { ...state };
  }
};
