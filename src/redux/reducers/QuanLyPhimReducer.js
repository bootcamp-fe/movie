import { SET_MOVIE_LIST, SET_CAROUSEL } from "../actions/constants/constants";

const initialState = {
  movieList: [],
  banner: [],
};

export const QuanLyPhimReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_CAROUSEL:
      return { ...state, banner: action.data };
    case SET_MOVIE_LIST:
      return { ...state, movieList: action.data };
    default:
      return { ...state };
  }
};
