import { SET_LOADING } from "../actions/constants/constants";

const initialState = {
  isLoading: false,
};

export const SpinnerReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_LOADING:
      return { ...state, isLoading: action.data };
    default:
      return { ...state };
  }
};
