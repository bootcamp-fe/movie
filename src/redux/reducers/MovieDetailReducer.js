import { SET_MOVIE_INFO } from "../actions/constants/constants";

const initialState = {
  movieInfo: {},
};

export const MovieDetailReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_MOVIE_INFO:
      return { ...state, movieInfo: action.data };

    default:
      return { ...state };
  }
};
