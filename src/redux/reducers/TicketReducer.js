import {
  SET_SELECTING_SEAT,
  SET_THEATER_INFO,
} from "../actions/constants/constants";

import _ from "lodash";

const initialState = {
  theaterInfo: {},
  selectedSeats: [],
  subtotal: 0,
};
export const TicketReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_THEATER_INFO: {
      return { ...state, theaterInfo: action.data };
    }
    case SET_SELECTING_SEAT: {
      let newSelectedSeats = [...state.selectedSeats];
      let selectedItemIndex = newSelectedSeats.findIndex((item) => {
        return item.maGhe === action.data.maGhe;
      });
      if (selectedItemIndex !== -1) {
        newSelectedSeats.splice(selectedItemIndex, 1);
      } else {
        newSelectedSeats.push(action.data);
      }
      let total = newSelectedSeats.reduce(
        (accumulator, currentValue) => accumulator + currentValue.giaVe,
        0
      );
      let sortedSelectedSeats = _.sortBy(
        newSelectedSeats,
        (item) => item.seatIndex
      );
      return { ...state, selectedSeats: sortedSelectedSeats, subtotal: total };
    }
    default:
      return { ...state };
  }
};
