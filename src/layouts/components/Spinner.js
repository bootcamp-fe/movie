import React from "react";
import { Dna } from "react-loader-spinner";
import { useSelector } from "react-redux";

export default function Spinner() {
  let { isLoading } = useSelector((state) => state.SpinnerReducer);
  return isLoading ? (
    <div className="fixed top-0 z-20 h-screen w-screen flex items-center justify-center bg-white">
      <Dna
        height="120"
        width="120"
        ariaLabel="progress-bar-loading"
        wrapperStyle={{}}
        wrapperClass="progress-bar-wrapper"
        borderColor="#01A0E3"
        barColor="#51E5FF"
      />
    </div>
  ) : (
    <></>
  );
}
