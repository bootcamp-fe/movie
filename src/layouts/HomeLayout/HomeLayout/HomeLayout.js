import { Route } from "react-router-dom";
import Header from "../components/Header";
import styles from "./HomeLayout.module.scss";
import { ProgressBar } from "react-loader-spinner";

export default function HomeLayout(props) {
  const { Component } = props;
  let headerHeight = "64px";
  return (
    <div className="">
      <div className="fixed z-10 top-0 bg-white w-full">
        <Header headerHeight={headerHeight} />
      </div>
      <div style={{ marginTop: headerHeight }} className="z-0">
        <Component />
      </div>
    </div>
  );
}
