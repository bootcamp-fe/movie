import React from "react";
import {
  DesktopView,
  MobileView,
  TabletView,
} from "~/layouts/Responsive/Responsive";
import HeaderDesktop from "./HeaderDesktop";
import HeaderMobile from "./HeaderMobile";

export default function Header({ headerHeight }) {
  return (
    <div>
      <DesktopView>
        <HeaderDesktop headerHeight={headerHeight} />
      </DesktopView>
      <TabletView>
        <HeaderDesktop headerHeight={headerHeight} />
      </TabletView>
      <MobileView>
        <HeaderMobile headerHeight={headerHeight} />
      </MobileView>
    </div>
  );
}
