import React, { useReducer } from "react";
import { localServ } from "~/services/localService";
import { message, Tooltip } from "antd";
import { faUser } from "@fortawesome/free-solid-svg-icons";
import { Link } from "react-router-dom";
import UserMenuTooltip from "./UserMenuTooltip";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowRightFromBracket } from "@fortawesome/free-solid-svg-icons";

export default function UserMenu({ mobile = false }) {
  let userInfo = localServ.user.get();
  const [ignored, forceUpdate] = useReducer((x) => x + 1, 0);
  let handleLogout = () => {
    localServ.user.remove();
    message.success("Logged out");
    forceUpdate();
    console.log("ok");
  };

  let GuestNav = () => {
    return (
      <div className="">
        <div className="flex items-center space-x-5">
          <Link
            to="/login"
            className="px-3 py-2 bg-blue-500 font-medium rounded text-white min-w-[100px] text-center hover:text-white hover:bg-blue-600 "
          >
            <span className="">Log in</span>
          </Link>
        </div>
      </div>
    );
  };
  let UserNav = () => {
    return (
      <div className="">
        <div className="flex items-center space-x-5">
          <Tooltip
            overlayStyle={{
              borderRadius: 1,
              backgroundColor: "transparent",
              padding: "16px 1px 1px 1px",
              cursor: "default",
            }}
            placement="bottom"
            color={"#FFFFFF"}
            title={<UserMenuTooltip handleLogout={handleLogout} />}
          >
            <span
              style={{ cursor: "default" }}
              className="text-lg font-medium flex items-center"
            >
              <span className="text-lg font-light mr-1">Xin chào </span>
              <span className="mr-3">{userInfo.username}</span>
              <FontAwesomeIcon
                color="rgb(59, 91, 152)"
                size="lg"
                icon={faUser}
              />
            </span>
          </Tooltip>
        </div>
      </div>
    );
  };
  let MobileUserNav = () => {
    return (
      <div className=" text-lg">
        <div className="mt-5 flex items-baseline justify-center">
          <span className="mr-2 font-semibold">Xin chào</span>
          <span className="mr-2  font-semibold text-blue-700">
            {userInfo.username}
          </span>
          <FontAwesomeIcon color="rgb(59, 91, 152)" size="xl" icon={faUser} />
        </div>
        <div className="flex justify-center mt-2">
          <button onClick={handleLogout} className="text-center">
            <span className="mr-2">Logout</span>
            <FontAwesomeIcon
              color="rgb(59, 91, 152)"
              size="lg"
              icon={faArrowRightFromBracket}
            />
          </button>
        </div>
      </div>
    );
  };
  let MobileGuestNav = () => {
    return (
      <div className=" text-lg">
        <div className="mt-5 flex items-baseline justify-center">
          <span className="mr-2 font-semibold ">Xin chào</span>
          <span className="mr-2 font-semibold text-blue-700 ">Guest</span>
          <FontAwesomeIcon color="rgb(59, 91, 152)" size="xl" icon={faUser} />
        </div>
        <div className="flex justify-center mt-2">
          <Link to={"/login"} className="text-center">
            <span className="mr-2">Login</span>
            <FontAwesomeIcon
              color="rgb(59, 91, 152)"
              size="lg"
              icon={faArrowRightFromBracket}
            />
          </Link>
        </div>
      </div>
    );
  };
  if (mobile) {
    return <>{userInfo ? <MobileUserNav /> : <MobileGuestNav />}</>;
  }
  return <>{userInfo ? <UserNav /> : <GuestNav />}</>;
}
