import React from "react";
import {
  UserOutlined,
  LogoutOutlined,
  SettingOutlined,
} from "@ant-design/icons";
import styled from "styled-components";
import { Divider } from "antd";

const MenuItem = styled.li`
  width: 160px;
  display: flex;
  align-items: center;
  cursor: pointer;
  background-color: white;
  font-size: 17px;
  font-weight: 500;
  padding: 5px 10px;
  border-radius: 4px;
  &:hover {
    background-color: #00a0e3;
    color: white;
  }
`;

export default function UserMenuTooltip({ handleLogout }) {
  return (
    <ul className="text-black pt-2 mb-0">
      <MenuItem>
        <UserOutlined />
        <span className="ml-3">View Profile</span>
      </MenuItem>
      <MenuItem>
        <SettingOutlined />
        <span className="ml-3">Setting</span>
      </MenuItem>
      <Divider style={{ margin: "6px 0" }} />
      <MenuItem onClick={handleLogout}>
        <LogoutOutlined />
        <span className="ml-3">Log Out</span>
      </MenuItem>
      {/* <li>
        <UserOutlined />
        View Profile
      </li>
      <li>Setting</li> */}
    </ul>
  );
}
