import React from "react";
import logo from "~/assets/images/logo.png";
import styles from "./Header.module.scss";
import UserMenu from "./UserMenu";
import { HashLink } from "react-router-hash-link";

export default function HeaderDesktop({ headerHeight }) {
  let navItems = [
    { label: "Lịch Chiếu", haskLink: "/#lichChieu" },
    { label: "Cụm Rạp", haskLink: "/#cumRap" },
    { label: "Tin Tức", haskLink: "/#tinTuc" },
    { label: "Ứng Dụng", haskLink: "/#ungDung" },
  ];

  let RenderNavItems = () => {
    return navItems.map((item, index) => {
      return (
        <li key={"navItem" + index + index} className="relative group ">
          <HashLink
            smooth
            className=" text-gray-900 group-hover:text-blue-500 "
            to={item.haskLink}
          >
            {item.label}
          </HashLink>
          <span className="absolute -bottom-2 left-1/2 w-0 h-1 bg-blue-500 group-hover:w-1/2 group-hover:transition-all"></span>
          <span className="absolute -bottom-2 right-1/2 w-0 h-1 bg-blue-500 group-hover:w-1/2 group-hover:transition-all"></span>
        </li>
      );
    });
  };
  return (
    <div
      style={{ height: headerHeight }}
      className={`${styles.header} w-full flex items-center justify-between px-5 `}
    >
      <div className="logo h-3/4">
        <a href="/">
          <img className="h-full" src={logo} alt="" />
        </a>
      </div>
      <ul className="navItem flex items-center space-x-8 mb-0  text-base font-medium ml-5">
        <RenderNavItems />
      </ul>
      <div className="userMenu">
        <UserMenu />
      </div>
    </div>
  );
}
