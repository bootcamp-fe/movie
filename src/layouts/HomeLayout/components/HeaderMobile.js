import React, { useRef } from "react";
import logo from "~/assets/images/logo.png";
import UserMenu from "./UserMenu";
import "./HeaderMobile.scss";
import { MenuOutlined, RightOutlined, LogoutOutlined } from "@ant-design/icons";

import { HashLink } from "react-router-hash-link";

export default function HeaderMobile({ headerHeight }) {
  let navItems = [
    { label: "Lịch Chiếu", haskLink: "/#lichChieu" },
    { label: "Cụm Rạp", haskLink: "/#cumRap" },
    { label: "Tin Tức", haskLink: "/#tinTuc" },
    { label: "Ứng Dụng", haskLink: "/#ungDung" },
  ];

  let menuModalRef = useRef();
  let sideBarRef = useRef();

  let handleShowMenu = () => {
    menuModalRef.current.classList.remove("hidden");
    setTimeout(() => {
      sideBarRef.current.classList.add("active");
    });
  };

  let handleHideMenu = (e) => {
    sideBarRef.current.classList.remove("active");
    sideBarRef.current.addEventListener(
      "transitionend",
      () => {
        menuModalRef.current.classList.add("hidden");
      },
      { once: true }
    );
  };

  return (
    <div
      style={{ height: headerHeight }}
      className="flex items-center px-2 justify-between relative"
    >
      <img src={logo} className="h-3/4" alt="" />
      <div onClick={handleShowMenu} className="menuBtn">
        <MenuOutlined style={{ fontSize: "24px", color: "#262626" }} />
      </div>
      <div
        onClick={handleHideMenu}
        ref={menuModalRef}
        className="sideBarModal hidden"
      >
        <div
          onClick={(e) => {
            e.stopPropagation();
          }}
          ref={sideBarRef}
          className="sideBarMenu  "
        >
          <div className="userInfo">
            <UserMenu mobile={true} />
          </div>
          <ul className="navBar text-center grow flex flex-col justify-around max-h-96">
            {navItems.map((item, index) => {
              return (
                <HashLink
                  key={"link" + index + index}
                  onClick={handleHideMenu}
                  smooth
                  className="py-3 text-lg font-semibold text-inherit"
                  to={item.haskLink}
                >
                  {item.label}
                </HashLink>
                // <li className="py-3 text-lg font-semibold">{item.label}</li>
              );
            })}
          </ul>
          <div className="text-center text-2xl my-5">
            <button onClick={handleHideMenu}>
              <RightOutlined />
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}
