import { BaseService } from "./BaseService";

class QuanLyNguoiDungService extends BaseService {
  constructor() {
    super();
  }

  dangNhap = (data) => {
    return this.post(`users/authenticate`, data);
  };
}

export const quanLyNguoiDungServ = new QuanLyNguoiDungService();
