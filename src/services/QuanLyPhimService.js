import { GROUP_ID } from "~/util/setting/config";

const { BaseService } = require("./BaseService");

export class QuanLyPhimService extends BaseService {
  constructor() {
    super();
  }
  layDanhSachBanner = () => {
    return this.get(`api/QuanLyPhim/LayDanhSachBanner`);
  };
  layDanhSachPhim = () => {
    return this.get(`api/QuanLyPhim/LayDanhSachPhim?maNhom=${GROUP_ID}`);
  };
  layLichChieuTheoPhim = (id) => {
    return this.get(`/api/QuanLyRap/LayThongTinLichChieuPhim?MaPhim=${id}`);
  };
}

export const quanLyPhimService = new QuanLyPhimService();
