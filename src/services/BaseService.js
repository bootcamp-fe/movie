import axios from "axios";
import { ACCESS_TOKEN, CYBERSOFT_TOKEN, DOMAIN } from "~/util/setting/config";
export class BaseService {
  put = (url, model) => {
    return axios({
      url: `${DOMAIN}/${url}`,
      method: "PUT",
      data: model,
      headers: { Authorization: "Bearer " + ACCESS_TOKEN },
    });
  };
  post = (url, model) => {
    return axios({
      url: `${DOMAIN}/${url}`,
      method: "POST",
      data: model,
      headers: {
        Authorization: "Bearer " + ACCESS_TOKEN,
        TokenCybersoft: CYBERSOFT_TOKEN,
      },
    });
  };
  get = (url) => {
    return axios({
      url: `${DOMAIN}/${url}`,
      method: "GET",
      headers: {
        Authorization: "Bearer " + ACCESS_TOKEN,
        TokenCybersoft: CYBERSOFT_TOKEN,
      },
    });
  };
  delete = (url) => {
    return axios({
      url: `${DOMAIN}/${url}`,
      method: "DELETE",
      headers: { Authorization: "Bearer " + ACCESS_TOKEN },
    });
  };
}
