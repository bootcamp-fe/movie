import { BaseService } from "./BaseService";

export class QuanLyDatVeService extends BaseService {
  constructor() {
    super();
  }
  layThongTinRapChieu = (maLichChieu) => {
    return this.get(
      `/api/QuanLyDatVe/LayDanhSachPhongVe?MaLichChieu=${maLichChieu}`
    );
  };
}

export const quanLyDatVeServ = new QuanLyDatVeService();
