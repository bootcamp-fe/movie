import "./App.css";
import "./index.css";
import { createBrowserHistory } from "history";
import { Router, BrowserRouter, Routes, Route } from "react-router-dom";
import HomePage from "./pages/Home/HomePage";
import HomeLayout from "./layouts/HomeLayout/HomeLayout/HomeLayout";
import "antd/dist/antd.min.css";
import "simplebar/dist/simplebar.min.css";
import MovieDetail from "./pages/MovieDetail/MovieDetail";
import Spinner from "./layouts/components/Spinner";
import LoginPage from "./pages/LoginPage/LoginPage";
import SignupPage from "./pages/SignupPage/SignupPage";
import BookingTicket from "./pages/BookingPage/BookingTicket";

export const history = createBrowserHistory();

function App() {
  return (
    <>
      <Spinner />
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<HomeLayout Component={HomePage} />} />
          <Route
            path="/movie-detail/:id"
            element={<HomeLayout Component={MovieDetail} />}
          />
          <Route path="/login" element={<LoginPage />} />
          <Route path="/sign-up" element={<SignupPage />} />
          <Route path="/ticket/:id" element={<BookingTicket />} />
        </Routes>
      </BrowserRouter>
    </>
  );
}

export default App;
