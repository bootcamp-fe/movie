import { Button, Checkbox, Divider, Form, Input, message, theme } from "antd";
import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import Lottie from "lottie-react";
import movie_animate from "../../assets/Lotties/lf20_cbrbre30.json";
// import { userServ } from "../../services/userService";
// import { localServ } from "../../services/localService";
import { UserOutlined, LockOutlined } from "@ant-design/icons";
import bgImg from "~/assets/images/login-bg.png";
import logoImg from "~/assets/images/logo.png";
import { quanLyNguoiDungServ } from "~/services/QuanLyNguoiDungService";
import { PostLogin } from "~/redux/actions/UserAction";

const LoginPage = () => {
  let dispatch = useDispatch();
  let navigate = useNavigate();
  let [loading, setLoading] = useState(false);

  let onLoginSuccess = () => {
    setLoading(false);
    message.success("Login Success");
    setTimeout(() => {
      navigate("/");
    }, 1000);
  };

  let onLoginFail = () => {
    setLoading(false);
    message.error("Login Fail");
  };

  const onFinish = (values) => {
    setLoading(true);
    console.log(values);
    dispatch(PostLogin(values, onLoginSuccess, onLoginFail));
  };

  //   const onFinishFailed = (errorInfo) => {
  //     setLoading(false);
  //     console.log("Failed:", errorInfo);
  //   };

  return (
    <div style={{ backgroundImage: `url(${bgImg})` }} className="">
      <div className="container mx-auto h-screen w-screen flex items-center justify-center ">
        <div
          style={{
            boxShadow: " rgba(0, 0, 0, 0.24) 0px 3px 8px",
            height: "40vw",
          }}
          className="w-3/4 flex  rounded-lg "
        >
          <div
            style={{ backgroundColor: "rgba(4, 86, 250, 0.7" }}
            className="w-1/2 rounded-lg flex items-center justify-center"
          >
            <div className="w-3/4">
              <Lottie animationData={movie_animate} />
            </div>
          </div>
          <div className="w-1/2 flex flex-col items-center justify-center ">
            <img
              className="animate-pulse"
              style={{ width: "20%" }}
              src={logoImg}
              alt=""
            />
            <p className="text-base md:text-lg lg:text-xl font-medium">
              Hello Again!
            </p>
            <Form
              className=" w-4/5"
              layout="vertical"
              name="basic"
              wrapperCol={{
                span: 24,
              }}
              initialValues={{}}
              onFinish={onFinish}
              //   onFinishFailed={onFinishFailed}
              autoComplete="off"
            >
              <Form.Item
                name="username"
                rules={[
                  { required: true, message: "Please input your Username!" },
                ]}
              >
                <Input
                  style={{
                    borderRadius: "5px",
                    height: "44px",
                  }}
                  prefix={<UserOutlined className="site-form-item-icon" />}
                  placeholder="Username"
                />
              </Form.Item>
              <Form.Item
                name="password"
                rules={[
                  { required: true, message: "Please input your Password!" },
                ]}
              >
                <Input
                  style={{
                    borderRadius: "5px",
                    height: "44px",
                  }}
                  prefix={<LockOutlined className="site-form-item-icon" />}
                  type="password"
                  placeholder="Password"
                />
              </Form.Item>
              <Form.Item
                wrapperCol={{
                  span: 24,
                }}
              >
                <Button
                  style={{
                    backgroundColor: "#365BBD",
                    borderRadius: "5px",
                    borderColor: "transparent",
                    color: "white",
                    display: "block",
                    height: "44px",
                    fontSize: "16px",
                    fontWeight: "500",
                  }}
                  className="w-full mt-5 "
                  type="ghost"
                  htmlType="submit"
                  loading={loading}
                >
                  Log In
                </Button>
              </Form.Item>
            </Form>
            <div className="flex justify-end w-3/4">
              <p>Don't have account yet?</p>
              <span
                onClick={() => {
                  navigate("/sign-up");
                }}
                className="ml-3 text-blue-500 cursor-pointer"
              >
                Sign up
              </span>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default LoginPage;
