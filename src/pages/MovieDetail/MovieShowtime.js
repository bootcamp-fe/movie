import React from "react";
import CinemaList from "../Home/CinemaList/CinemaList";
import ShowtimeTable from "./components/ShowtimeTable/ShowtimeTable";

export default function MovieShowtime({ movieInfo }) {
  return (
    <div id="showTime" className="bg-[#0A2029] lg:px-10">
      <div className="container mx-auto  pt-5 pb-10">
        <h3 className="text-xl font-semibold text-[#FB4226] text-left pl-5">
          LỊCH CHIẾU
        </h3>
        <ShowtimeTable movieInfo={movieInfo} />
      </div>
    </div>
  );
}
