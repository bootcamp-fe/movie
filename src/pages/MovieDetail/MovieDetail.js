import React from "react";
import TrailerModal from "../Home/TrailerModal";
import MovieDescription from "./MovieDescription";
import MovieShowtime from "./MovieShowtime";
import { useEffect } from "react";
import { getShowTimeById } from "~/redux/actions/MovieDetailAction";
import { useParams } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import MovieInfo from "./components/MovieInfo/MovieInfo";

export default function MovieDetail() {
  let movieInfo = useSelector((state) => state.MovieDetailReducer.movieInfo);
  let params = useParams();
  let dispatch = useDispatch();
  console.log(movieInfo);
  useEffect(() => {
    dispatch(getShowTimeById(params.id));
  }, []);
  return (
    <div className="">
      <TrailerModal />
      <MovieInfo movieInfo={movieInfo} />
      <MovieDescription movieInfo={movieInfo} />
      <MovieShowtime movieInfo={movieInfo} />
    </div>
  );
}
