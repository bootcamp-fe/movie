import moment from "moment";
import React from "react";
import { useDispatch } from "react-redux";
import playBtn from "~/assets/images/play-video.png";
import { OPEN_TRAILER_MODAL } from "~/redux/actions/constants/constants";

export default function MovieInfoMobile({ movieInfo }) {
  let dispatch = useDispatch();
  const handlePlayTrailer = (e) => {
    e.preventDefault();
    dispatch({ type: OPEN_TRAILER_MODAL, data: movieInfo.trailer });
  };
  return (
    <div>
      <div className="trailer">
        <div
          style={{
            backgroundImage: `url(${movieInfo.hinhAnh})`,
            height: "50vw",
            backgroundPosition: "center",
            backgroundRepeat: "no-repeat",
            backgroundSize: "cover",
          }}
          className="group"
          alt="movie-img"
        >
          <div className="playTrailer opacity-100 transition-opacity ease-in-out duration-300  w-full h-full flex items-center justify-center bg-gradient-to-t from-[#0A2029]  ">
            <button onClick={handlePlayTrailer} className="w-1/4">
              <img
                className="aspect-square mx-auto  opacity-60 hover:opacity-95"
                src={playBtn}
                alt=""
              />
            </button>
          </div>
        </div>
      </div>
      <div className="info pl-5 py-5 text-white bg-[#0A2029] ">
        <p className="">
          <span className="pr-1">Ngày khởi chiếu:</span>
          {moment(movieInfo.ngayKhoiChieu).format("YYYY-MM-DD")}
        </p>
        <p className="m-0">
          {movieInfo.hot === true ? (
            <span className="mr-2 px-2 py-1 rounded text-white font-bold bg-red-500 ">
              Hot
            </span>
          ) : (
            ""
          )}

          <span className=" text-lg font-bold">{movieInfo.tenPhim}</span>
        </p>
      </div>
    </div>
  );
}
