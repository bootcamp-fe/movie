import React from "react";
import playBtn from "~/assets/images/play-video.png";
import { Link, useParams } from "react-router-dom";
import { Rate } from "antd";
import { useDispatch } from "react-redux";
import { OPEN_TRAILER_MODAL } from "~/redux/actions/constants/constants";

import moment from "moment/moment";

export default function MovieInfoDesktop({ movieInfo }) {
  let dispatch = useDispatch();
  const handlePlayTrailer = (e) => {
    e.preventDefault();
    dispatch({ type: OPEN_TRAILER_MODAL, data: movieInfo.trailer });
  };
  return (
    <div
      style={{
        height: "41vw",
        maxHeight: "760px",
      }}
      className="movieDetail relative "
    >
      <div
        style={{
          backgroundImage: `url(${movieInfo.hinhAnh})`,
          backgroundSize: "cover",
          backgroundPosition: "center",
          backgroundColor: "#ccc",
          filter: "blur(8px)",
        }}
        className="h-full w-full "
      ></div>
      <div className=" absolute top-0 right-0 left-0 bottom-0 container mx-auto h-full flex items-center justify-between lg:px-10">
        <div
          style={{ backgroundColor: "rgba(136, 136, 136, 0.49)" }}
          className="info flex items-center text-white text-base p-5 rounded "
        >
          <div className="relative group">
            <div className="playTrailer absolute group-hover:opacity-100 transition-opacity ease-in-out duration-300 opacity-0 w-full h-full flex items-center justify-center bg-gradient-to-t hover:from-[#3233334f]  ">
              <button className="w-1/4">
                <img
                  onClick={handlePlayTrailer}
                  className="aspect-square  opacity-60 hover:opacity-95"
                  src={playBtn}
                  alt=""
                />
              </button>
            </div>
            <img
              className="w-32 sm:w-40 md:w-54 lg:w-56 rounded "
              src={movieInfo.hinhAnh}
              alt=""
            />
          </div>
          <div className="detail ml-4">
            <p className="">
              <span className="pr-1">Ngày khởi chiếu:</span>
              {moment(movieInfo.ngayKhoiChieu).format("YYYY-MM-DD")}
            </p>
            <p>
              {movieInfo.hot === true ? (
                <span className="mr-2 px-2 py-1 rounded text-white font-bold bg-red-500 ">
                  Hot
                </span>
              ) : (
                ""
              )}

              <span className=" text-lg font-bold">{movieInfo.tenPhim}</span>
            </p>
            <p className="text-right">120 phút - 2D</p>
            <div className="flex justify-end mt-10">
              <a
                href="#showTime"
                className="px-5 py-1 rounded text-white text-lg font-semibold bg-emerald-600"
              >
                Đặt vé
              </a>
            </div>
          </div>
        </div>
        <div className="rating flex flex-col items-center ">
          <div
            style={{
              border: "solid 10px #7ED321 ",
              borderRadius: "50%",
              backgroundColor: "rgba(136, 136, 136, 0.49)",
            }}
            className="w-24 sm:w-36 md:w-40 aspect-square flex items-center justify-center"
          >
            <p className="m-0 text-white font-bold text-3xl">
              {movieInfo.danhGia}
            </p>
          </div>
          <div
            style={{ backgroundColor: "rgba(136, 136, 136, 0.49)" }}
            className="star text-center pb-1 px-2 mt-2 rounded"
          >
            <Rate value={movieInfo.danhGia / 2} disabled />
          </div>
        </div>
      </div>
    </div>
  );
}
