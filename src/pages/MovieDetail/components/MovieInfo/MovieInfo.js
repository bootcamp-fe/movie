import React from "react";
import { DefaultView, MobileView } from "~/layouts/Responsive/Responsive";
import MovieInfoDesktop from "./MovieInfoDesktop";
import MovieInfoMobile from "./MovieInfoMobile";

export default function MovieInfo({ movieInfo }) {
  return (
    <div className="">
      <DefaultView>
        <MovieInfoDesktop movieInfo={movieInfo} />
      </DefaultView>
      <MobileView>
        <MovieInfoMobile movieInfo={movieInfo} />
      </MobileView>
    </div>
  );
}
