import { Empty, Tabs } from "antd";
import _ from "lodash";
import moment from "moment/moment";
import React from "react";
import "moment/locale/vi";
import { Link } from "react-router-dom";

export default function ShowtimeTableMobile({ movieInfo }) {
  let { heThongRapChieu } = movieInfo;
  let heThongRap = heThongRapChieu?.map((item) => {
    return { maHeThongRap: item.tenHeThongRap, logo: item.logo };
  });

  let lichChieuTheoRap = (lichChieu) => {
    let danhSachNgayChieu = _.uniq(
      lichChieu.map((item) =>
        moment(item.ngayChieuGioChieu).format("DD-MM-YYYY")
      )
    );
    console.log(danhSachNgayChieu);
    return (
      <div className="">
        {danhSachNgayChieu.map((date, index) => {
          return (
            <div
              key={"ngayChieu" + index + index}
              className=" rounded bg-slate-300 py-2 px-2"
            >
              <p className="capitalize text-base font-bold pl-1">
                {moment(date, "DD-MM-YYYY").locale("vi").format("dddd, DD-MM")}
              </p>
              <div className="grid grid-cols-4 gap-4">
                {lichChieu.map((item) => {
                  if (
                    moment(item.ngayChieuGioChieu).format("DD-MM-YYYY") === date
                  ) {
                    return (
                      <Link
                        to={`/ticket/${item.maLichChieu}`}
                        key={"gioChieu" + index + index}
                        className="px-2 hover:text-white bg-blue-500 py-2 rounded text-white font-semibold text-center tracking-widest	"
                      >
                        {moment(item.ngayChieuGioChieu).format("HH:mm")}
                      </Link>
                    );
                  }
                })}
              </div>
            </div>
          );
        })}
      </div>
    );
  };

  let cumRap = (maHT) => {
    let cumRap = heThongRapChieu.find((item) => item.tenHeThongRap == maHT);
    let cumRapLabel = cumRap.cumRapChieu.map((item) => {
      let LichChieuPhim = lichChieuTheoRap(item.lichChieuPhim);
      return {
        label: item.tenCumRap,
        key: item.maCumRap,
        children: LichChieuPhim,
      };
    });
    return cumRapLabel;
  };
  let heThongRapTab = heThongRap?.map((item) => {
    let cumRapLabel = cumRap(item.maHeThongRap);
    console.log(cumRapLabel);
    return {
      label: item.maHeThongRap,
      key: item.maHeThongRap,
      children: <Tabs items={cumRapLabel} />,
    };
  });
  return (
    <div
      style={{ backgroundColor: "rgba(255, 255, 255, 0.9)" }}
      className=" rounded px-5 py-5"
    >
      {_.isEmpty(heThongRapChieu) ? (
        <div className="py-2">
          <Empty
            image={Empty.PRESENTED_IMAGE_SIMPLE}
            description="Chưa có thông tin lịch chiếu !"
          />
        </div>
      ) : (
        <Tabs items={heThongRapTab} />
      )}
    </div>
  );
}
