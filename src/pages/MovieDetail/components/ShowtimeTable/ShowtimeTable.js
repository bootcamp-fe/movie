import React from "react";
import {
  DesktopView,
  MobileView,
  TabletView,
} from "~/layouts/Responsive/Responsive";
import ShowtimeTableDesktop from "./ShowtimeTableDesktop";
import ShowtimeTableMobile from "./ShowtimeTableMobile";
import ShowtimeTableTablet from "./ShowtimeTableTablet";

export default function ShowtimeTable({ movieInfo }) {
  return (
    <div>
      <DesktopView>
        <ShowtimeTableDesktop movieInfo={movieInfo} />
      </DesktopView>
      <TabletView>
        <ShowtimeTableTablet movieInfo={movieInfo} />
      </TabletView>
      <MobileView>
        <ShowtimeTableMobile movieInfo={movieInfo} />
      </MobileView>
    </div>
  );
}
