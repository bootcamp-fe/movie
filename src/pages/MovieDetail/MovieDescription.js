import moment from "moment";
import React from "react";

export default function MovieDescription({ movieInfo }) {
  return (
    <div className="bg-[#0A2029] lg:px-10">
      <div className="container mx-auto  pt-5">
        <h3 className="text-xl font-semibold text-[#FB4226] text-left pl-5">
          THÔNG TIN
        </h3>
        <div className="grid grid-cols-1 lg:grid-cols-2 pt-2">
          <div className="info">
            <table className="border-spacing-x-5 border-spacing-y-2 border-separate">
              <tbody>
                <tr>
                  <td className="w-32 text-white font-semibold text-base">
                    Ngày công chiếu
                  </td>
                  <td className="text-white">
                    {moment(movieInfo.ngayKhoiChieu).format("DD-MM-YYYY")}
                  </td>
                </tr>
                <tr>
                  <td className="text-white font-semibold text-base">
                    Đạo diễn
                  </td>
                  <td className="text-white">Director</td>
                </tr>
                <tr>
                  <td className="text-white font-semibold text-base">
                    Diễn Viên
                  </td>
                  <td className="text-white">
                    Actress , Actors, Actress , Actors
                  </td>
                </tr>
                <tr>
                  <td className="text-white font-semibold text-base">
                    Thể loại
                  </td>
                  <td className="text-white">
                    Hành động, Giả tưởng, Ly kỳ, Thần thoại
                  </td>
                </tr>
                <tr>
                  <td className="text-white font-semibold text-base">
                    Định dạng
                  </td>
                  <td className="text-white">2D/Digital</td>
                </tr>
                <tr>
                  <td className="text-white font-semibold text-base">
                    Quốc Gia
                  </td>
                  <td className="text-white">Country name</td>
                </tr>
              </tbody>
            </table>
          </div>
          <div className="desc pl-5 lg:pl-0 mt-3 lg:mt-0">
            <h4 className="text-lg font-semibold text-white">Nội dung</h4>
            <p className="text-white">{movieInfo.moTa}</p>
          </div>
        </div>
      </div>
    </div>
  );
}
