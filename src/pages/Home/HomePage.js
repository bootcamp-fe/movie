import React from "react";
import CinemaList from "./CinemaList/CinemaList";
import HomeCarousel from "./HomeCarousel";
import MovieList from "./MovieList";

export default function HomePage() {
  return (
    <div className="">
      <HomeCarousel />
      <div className="container mx-auto pb-96">
        <MovieList />
        <CinemaList />
      </div>
    </div>
  );
}
