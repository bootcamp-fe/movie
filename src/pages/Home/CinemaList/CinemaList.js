import React from "react";
import { DefaultView, MobileView } from "~/layouts/Responsive/Responsive";
import CinemaListDefault from "./CinemaListDefault";
import CinemaListMobile from "./CinemaListMobile";
import { data } from "~/model/LichChieuTheoCumRap";

export default function CinemaList() {
  return (
    <div id="cumRap">
      <DefaultView>
        <CinemaListDefault data={data} />
      </DefaultView>
      <MobileView>
        <CinemaListMobile data={data} />
      </MobileView>
    </div>
  );
}
