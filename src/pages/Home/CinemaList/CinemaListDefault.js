import { Tabs } from "antd";
import _ from "lodash";
import moment from "moment";
import React, { useEffect, useState } from "react";
import SimpleBar from "simplebar-react";

export default function CinemaListDefault({ data }) {
  const [tabPos, setTabPos] = useState("left");
  useEffect(() => {
    window.screen.width <= 760 ? setTabPos("top") : setTabPos("left");
  }, [window.screen.width]);

  let labelHeThongRapTab = (heThongRap) => {
    return (
      <div key={heThongRap.maHeThongRap} className="">
        <img
          className=" 2xl:w-20 xl:w-16 lg:w-14 md:w-12 sm:w-10 max-w-none w-10 "
          src={heThongRap.logo}
          alt=""
        />
      </div>
    );
  };
  let labelRap = (rap) => {
    return (
      <div
        key={rap.maCumRap}
        className="flex items-center 2xl:w-full xl:w-full lg:w-full md:w-44 sm:w-48 w-32"
      >
        <img
          className="2xl:w-16 md:w-14 sm:hidden md:block hidden"
          src={rap.hinhAnh}
          alt=""
        />
        <p className="mb-0 md:ml-2 ml-5 truncate text-lg">{rap.tenCumRap}</p>
      </div>
    );
  };
  let labelPhim = (phim) => {
    return (
      <div
        key={phim.maPhim}
        className="flex sm:flex-col md:flex-row items-center 2xl:w-full xl:w-full lg:w-full md:w-44 sm:w-28 w-20 flex-col"
      >
        <img
          className="2xl:w-16 md:w-14   sm:w-20 w-14"
          src={phim.hinhAnh}
          alt=""
        />
        <p className="mb-0 text-left text-lg ml-5 md:ml-5 sm:ml-0 sm:mt-1 sm:text-center md:w-36 xl:w-60 truncate w-full">
          {phim.tenPhim}
        </p>
      </div>
    );
  };
  // Render cinema chain
  let cinemaChain = data.content.map((item) => {
    // Render Cinema List
    let cinemaList = item.lstCumRap.map((item) => {
      // Render Movie List
      let danhSachPhimTheoRap = item.danhSachPhim.slice(0, 10).map((items) => {
        let monthsOfShowtime = items.lstLichChieuTheoPhim.map((item) =>
          moment(item.ngayChieuGioChieu).format("MM")
        );

        let uniqMonth = _.sortBy(_.uniq(monthsOfShowtime));

        let showtime = () => {
          return (
            <>
              {uniqMonth.map((item, index) => {
                let thisMonthSchedule = items.lstLichChieuTheoPhim.filter(
                  (lichChieu) =>
                    moment(lichChieu.ngayChieuGioChieu).format("MM") === item
                );
                let uniqDay = _.sortBy(
                  _.uniq(
                    thisMonthSchedule.map((item) => {
                      return moment(item.ngayChieuGioChieu).format(
                        "dddd, [ngày] DD"
                      );
                    })
                  )
                );
                return (
                  <div key={"lcp" + index + index.toString()} className="mt-2">
                    <p className="text-lg text-gray-300 font-bold">
                      Tháng {item}
                    </p>
                    {uniqDay.map((dayname, index) => {
                      let thisDaySchedule = thisMonthSchedule.filter(
                        (lichChieu) => {
                          return (
                            moment(lichChieu.ngayChieuGioChieu).format(
                              "dddd, [ngày] DD"
                            ) === dayname
                          );
                        }
                      );
                      return (
                        <div
                          key={"ncp" + index + index.toString()}
                          style={{ borderTop: "solid 1px #ccc" }}
                          className=" flex items-center sm:flex-col md:flex-row pt-2 mt-2 mr-2"
                        >
                          <span
                            className="mr-4 sm:mr-0 md:mr-2 pr-1 text-lg
                           font-semibold capitalize"
                          >
                            {dayname}
                          </span>
                          <div className="grow grid  md:grid-cols-2 2xl:grid-cols-4 md:py-2 lg:grid-cols-4 sm:grid-cols-2 sm:w-full md:w-min  sm:text-center pr-2 gap-4">
                            {thisDaySchedule.map((item, index) => {
                              return (
                                <div
                                  key={"gcp" + index + index.toString}
                                  className="bg-blue-500 text-center md:py-2 sm:py-2 sm:mt-2  text-white px-2 py-1 font-bold rounded"
                                >
                                  {moment(item.ngayChieuGioChieu).format(
                                    "HH:mm"
                                  )}
                                </div>
                              );
                            })}
                          </div>
                        </div>
                      );
                    })}
                  </div>
                );
              })}
            </>
          );
        };
        // return list film
        return {
          label: labelPhim(items),
          key: items.maPhim,
          children: (
            <SimpleBar style={{ height: "100%" }}>{showtime()}</SimpleBar>
          ),
        };
      });
      // Return Cinema List
      return {
        label: labelRap(item),
        key: item.maCumRap,
        children: (
          <SimpleBar>
            <Tabs
              // style={{ height: "720px" }}
              tabPosition={tabPos}
              items={danhSachPhimTheoRap}
            />
          </SimpleBar>
        ),
      };
    });
    // Return Cinema Chain
    return {
      label: labelHeThongRapTab(item),
      key: item.maHeThongRap,
      children: (
        <Tabs
          // style={{ height: "720px" }}
          tabPosition={tabPos}
          items={cinemaList}
        />
      ),
    };
  });
  return (
    <div className="mt-5">
      <h3 className="text-center">Cinema List</h3>
      <div className="">
        <Tabs centered tabPosition="top" items={cinemaChain} />
      </div>
    </div>
  );
}
