import { Select, Tabs } from "antd";
import _ from "lodash";
import moment from "moment";
import React from "react";
import { useState } from "react";
import { mockComponent } from "react-dom/test-utils";
import { Link } from "react-router-dom";

export default function CinemaListMobile({ data }) {
  // Mảng danh sách hệ thống rạp và thông tin cụm rạp
  let heThongRap = data.content.map((item) => {
    return {
      maHeThongRap: item.maHeThongRap,
      tenHeThongRap: item.tenHeThongRap,
      logo: item.logo,
      listCumRap: item.lstCumRap,
    };
  });
  // Component Cụm Rạp Trong Hệ Thống Rạp
  let CumRap = ({ listCumRap }) => {
    let [danhSachPhim, setDanhSachPhim] = useState();
    let handleSelectionChange = (value) => {
      let danhSachPhim = listCumRap.find((item) => {
        return item.maCumRap === value;
      }).danhSachPhim;
      setDanhSachPhim(danhSachPhim);
    };
    return (
      <div className="">
        {/* Tạo danh sách chọn các rạp */}
        <Select
          onChange={handleSelectionChange}
          placeholder="Chọn rạp"
          style={{ width: "100%" }}
          options={listCumRap.map((item) => {
            return {
              value: item.maCumRap,
              label: item.tenCumRap,
            };
          })}
        />
        <div style={{ maxHeight: 400, overflow: "scroll" }} className="mt-3">
          {/* Tạo danh sách phim theo rạp */}
          {danhSachPhim?.map((item, index) => {
            // Lấy danh sách ngày chiếu theo phim
            let ngayChieu = _.uniq(
              item.lstLichChieuTheoPhim.map((item) =>
                moment(item.ngayChieuGioChieu).format("dddd, [Ngày] DD-MM")
              )
            );
            return (
              <div
                style={{ boxShadow: "rgba(0, 0, 0, 0.16) 0px 1px 4px" }}
                key={"phim" + index + index}
                className="my-4 mx-1 px-2 py-5 rounded flex bg-gray-100 "
              >
                <div
                  style={{
                    width: "100px",
                    aspectRatio: "3/4",
                    backgroundImage: `url(${item.hinhAnh})`,
                    backgroundPosition: "center",
                    backgroundSize: "cover",
                  }}
                  className="rounded"
                ></div>
                <div className="ml-3 grow">
                  <div className="flex justify-between items-center">
                    <span className="font-bold mb-1">{item.tenPhim}</span>
                    {item.hot ? (
                      <span className="font-semibold bg-red-500 rounded px-2  text-white">
                        Hot
                      </span>
                    ) : (
                      ""
                    )}
                  </div>
                  <div className=" h-32 overflow-scroll border border-solid bg-gray-50 border-gray-300 px-1 py-2 rounded ">
                    {ngayChieu.map((ngay) => {
                      let danhSachPhimTheoNgay =
                        item.lstLichChieuTheoPhim.filter(
                          (lichChieu) =>
                            moment(lichChieu.ngayChieuGioChieu).format(
                              "dddd, [Ngày] DD-MM"
                            ) == ngay
                        );
                      return (
                        <div key={"ngayChieu" + ngay + ngay} className="pt-2">
                          <p className="capitalize mb-2">{ngay}</p>
                          <div className="w-full  grid grid-cols-2 gap-2">
                            {danhSachPhimTheoNgay.map((phim, index) => {
                              return (
                                <Link
                                  key={"gioChieu" + index + index}
                                  className=" px-2 py-2 bg-blue-400 hover:text-white rounded block text-white text-center font-semibold text-base "
                                >
                                  {moment(phim.ngayChieuGioChieu).format(
                                    "hh[h]mm"
                                  )}
                                </Link>
                              );
                            })}
                          </div>
                        </div>
                      );
                    })}
                  </div>
                </div>
              </div>
            );
          })}
        </div>
      </div>
    );
  };

  // Label Hệ Thống Rạp dùng cho Tab
  let heThongRapLabel = heThongRap.map((item, index) => {
    return {
      label: <img className="w-40" src={item.logo} alt="" />,
      key: item + "-" + index,
      children: <CumRap listCumRap={item.listCumRap} />,
    };
  });
  return (
    <div className="px-5">
      <Tabs className="" centered items={heThongRapLabel} />
    </div>
  );
}
