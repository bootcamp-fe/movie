import React from "react";
import doctor from "~/assets/images/doctor.jpg";
import playBtn from "~/assets/images/play-video.png";
import { Link } from "react-router-dom";
import { Rate } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { OPEN_TRAILER_MODAL } from "~/redux/actions/constants/constants";

export default function MovieCard({ movieInfo }) {
  let dispatch = useDispatch();

  const handlePlayTrailer = (e) => {
    e.preventDefault();
    dispatch({ type: OPEN_TRAILER_MODAL, data: movieInfo.trailer });
  };
  return (
    <div
      style={{ boxShadow: "rgba(99, 99, 99, 0.2) 0px 2px 8px 0px" }}
      className="group rounded overflow-hidden cursor-pointer relative w-full"
    >
      <div className="rating w-20 absolute right-2 top-2 flex flex-col items-center py-1 rounded opacity-80 justify-center bg-[#1F2C3A]">
        <p className="text-white m-0 text-base font-bold">
          {movieInfo.danhGia / 2}
        </p>
        <Rate
          style={{ fontSize: "10px", letterSpacing: "1px" }}
          disabled
          allowHalf
          defaultValue={movieInfo.danhGia / 2}
        />
      </div>
      <Link to={`/movie-detail/${movieInfo.maPhim}`}>
        <div
          style={{
            backgroundImage: `url("${movieInfo.hinhAnh}")`,
            backgroundPosition: "center",
            backgroundSize: "cover",
          }}
          className="  cardImg w-full h-64 2xl:h-80 :"
        >
          <div className="playTrailer group-hover:opacity-100 transition-opacity ease-in-out duration-300 opacity-0 w-full h-full flex items-center justify-center bg-gradient-to-t hover:from-[#3233334f]  ">
            <button onClick={handlePlayTrailer} className="w-1/4">
              <img
                className="aspect-square mx-auto opacity-60 hover:opacity-95"
                src={playBtn}
                alt=""
              />
            </button>
          </div>
        </div>
      </Link>
      <div className="cardFooter h-20 relative">
        <div className="buyTicket  group-hover:opacity-100 opacity-0 transition-opacity ease-in-out duration-300 absolute bg-white top-0 h-full w-full flex items-center justify-center px-2">
          <Link
            className=" bg-[#FB4226] rounded font-semibold md:text-lg  text-2xl w-full h-1/2 text-white hover:text-white flex items-center justify-center"
            to={`/movie-detail/${movieInfo.maPhim}`}
          >
            <p className="m-0">CHI TIẾT</p>
          </Link>
        </div>
        <div className="ml-2 movieName pt-2">
          <div className="flex items-center pr-2">
            <div className="font-bold md:text-sm md:ml-1 truncate  text-lg">
              {movieInfo.tenPhim}
            </div>
            {movieInfo.hot ? (
              <span className=" ml-2 px-2 py-1 rounded text-white font-bold bg-red-500 ">
                Hot
              </span>
            ) : (
              ""
            )}
          </div>

          <p className="pt-3">120 phút - tix 10</p>
        </div>
      </div>
    </div>
  );
}

//
