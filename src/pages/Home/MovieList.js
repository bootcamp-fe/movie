import { Tabs } from "antd";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import MovieCarousel from "./MovieCarousel";
import TrailerModal from "./TrailerModal";
import { setMovieList } from "~/redux/actions/MovieListAction";

export default function MovieList() {
  let dispatch = useDispatch();
  let movieList = useSelector((state) => state.QuanLyPhimReducer.movieList);
  let listPhimDangChieu = movieList.filter((item) => item.dangChieu == true);
  let listPhimSapChieu = movieList.filter((item) => item.sapChieu == true);

  useEffect(() => {
    dispatch(setMovieList);
  }, []);

  const items = [
    {
      label: "Đang chiếu",
      key: "item-1",
      children: <MovieCarousel renderList={listPhimDangChieu} />,
    }, // remember to pass the key prop
    {
      label: "Sắp chiếu",
      key: "item-2",
      children: <MovieCarousel renderList={listPhimSapChieu} />,
    },
  ];
  return (
    <div id="lichChieu" className="mt-5">
      <TrailerModal />
      <Tabs size="large" centered defaultActiveKey="1" items={items}></Tabs>
    </div>
  );
}
