import { Modal } from "antd";
import React from "react";
import { CloseCircleOutlined } from "@ant-design/icons";
import { useDispatch, useSelector } from "react-redux";
import { CLOSE_TRAILER_MODAL } from "~/redux/actions/constants/constants";
export default function TrailerModal() {
  let modalState = useSelector((state) => {
    return state.HomepageModalReducer;
  });
  let { isVideoModalOpen, youtubeId } = modalState;
  let dispatch = useDispatch();
  const handleCloseModal = () => {
    dispatch({ type: CLOSE_TRAILER_MODAL });
  };
  return (
    <Modal
      onCancel={handleCloseModal}
      width={"80%"}
      bodyStyle={{ padding: 0, width: "100%", paddingTop: "56.25%" }}
      footer={null}
      centered
      open={isVideoModalOpen}
      closable={false}
      destroyOnClose
    >
      <iframe
        style={{
          position: "absolute",
          top: 0,
          left: 0,
          bottom: 0,
          right: 0,
          width: "100%",
          height: "100%",
        }}
        src={`https://www.youtube.com/embed/${youtubeId}`}
        title="Trailer"
        frameBorder="0"
        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
        allowFullScreen
      ></iframe>
      <button
        onClick={handleCloseModal}
        className="absolute -top-4 -right-4 text-white"
      >
        <CloseCircleOutlined className="text-4xl" />
      </button>
    </Modal>
  );
}
