import React, { useEffect } from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import "./swipperStyle.scss";
import "swiper/css";
import "swiper/css/navigation";
import "swiper/css/pagination";
import { Navigation, Pagination, Autoplay } from "swiper";
import { useDispatch, useSelector } from "react-redux";
import { getCarouselImg } from "~/redux/actions/CarouselActions";

export default function HomeCarousel() {
  let dispatch = useDispatch();
  let bannerArr = useSelector((state) => state.QuanLyPhimReducer.banner);
  useEffect(() => {
    dispatch(getCarouselImg());
  }, []);
  let renderBanner = () => {
    return bannerArr.map((item, index) => {
      return (
        <SwiperSlide key={index + index.toString()}>
          <div
            style={{
              height: "100%",
              width: "100%",
            }}
          >
            <img
              style={{ width: "100%", height: "100%" }}
              src={item.hinhAnh}
              alt=""
            />
          </div>
        </SwiperSlide>
      );
    });
  };
  return (
    <Swiper
      style={{ maxHeight: "830px" }}
      autoplay={{
        delay: 3000,
        disableOnInteraction: false,
      }}
      navigation={true}
      modules={[Navigation, Pagination, Autoplay]}
      pagination={true}
    >
      {renderBanner()}
    </Swiper>
  );
}
