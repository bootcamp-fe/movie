import React, { useRef, useState } from "react";
// Import Swiper React components
import { Swiper, SwiperSlide } from "swiper/react";

// Import Swiper styles
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/navigation";

// import required modules
import { Pagination, Navigation } from "swiper";

import MovieCard from "./MovieCard";

export default function MovieCarousel({ renderList }) {
  const cardPerSwiper = 8;

  let renderSlides = () => {
    return renderList.map((item, index) => {
      let slideOrder = index + 1;
      if (slideOrder == 1 || slideOrder % cardPerSwiper == 0) {
        return (
          <SwiperSlide key={"slide" + index + index}>
            <div className="flex-grow grid md:grid-cols-3 lg:grid-cols-4 sm:grid-cols-2 2xl:px-28 xl:px-20 lg:px-16 md:px-12 sm:px-16 gap-10  mt-5 pb-14 w-1/3 px-5 md:px-0">
              {renderList
                .slice(index, index + cardPerSwiper)
                .map((item, index) => {
                  return (
                    <MovieCard key={"card" + index + index} movieInfo={item} />
                  );
                })}
            </div>
          </SwiperSlide>
        );
      }
    });
  };
  return (
    <>
      <Swiper
        pagination={true}
        navigation={true}
        modules={[Pagination, Navigation]}
        className="mySwiper"
      >
        {renderSlides()}
      </Swiper>
    </>
  );
}
