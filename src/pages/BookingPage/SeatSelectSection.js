import moment from "moment/moment";
import React, { useReducer } from "react";
import { useDispatch, useSelector } from "react-redux";
import "./SeatSelect.scss";
import screenImg from "~/assets/images/screen.png";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faXmark, faCouch } from "@fortawesome/free-solid-svg-icons";
import chairImg from "~/assets/images/couch.png";
import reservedChairImg from "~/assets/images/reserved-couch.png";
import vipChairImg from "~/assets/images/vip-couch.png";
import { SET_SELECTING_SEAT } from "~/redux/actions/constants/constants";

const NORMAL_SEAT = "Thuong";
const VIP_SEAT = "Vip";

export default function SeatSelectSection({ handleChangeView }) {
  let theaterInfo = useSelector((state) => state.TicketReducer.theaterInfo);
  let { thongTinPhim = "", danhSachGhe = [] } = theaterInfo;
  let { selectedSeats, subtotal } = useSelector((state) => state.TicketReducer);
  let dispatch = useDispatch();

  let renderSeatsInRow = (startIndex, endIndex, rowLetter) => {
    let thisRowSeatData = danhSachGhe.slice(startIndex, endIndex);
    return thisRowSeatData.map((seatData, index) => {
      let columIndex = index + 1;
      let seatIndex = rowLetter + columIndex;
      return (
        <td
          key={seatData.maGhe}
          className={seatData.daDat == false ? "cursor-pointer" : ""}
        >
          <img
            onClick={
              seatData.daDat == false
                ? (e) => {
                    handleSeatSelection(e, seatData, seatIndex);
                  }
                : null
            }
            className={
              selectedSeats.findIndex((item) => item.maGhe == seatData.maGhe) !=
              -1
                ? "active"
                : ""
            }
            src={
              seatData.daDat
                ? reservedChairImg
                : seatData.loaiGhe == NORMAL_SEAT
                ? chairImg
                : vipChairImg
            }
          />
        </td>
      );
    });
  };

  let renderSeatRow = () => {
    return (
      <>
        {danhSachGhe.map((item, index) => {
          if (index % 16 == 0 || index % 16 == 16) {
            let thisRowStartIndex = index;
            let thisRowEndIndex = index + 16;
            let rowIndex = thisRowStartIndex / 16;
            let letter = String.fromCharCode(rowIndex + "A".charCodeAt(0));
            return (
              <tr key={"seatRow" + index + index}>
                <td key={"rowHead" + index + index}>{letter}</td>
                {renderSeatsInRow(thisRowStartIndex, thisRowEndIndex, letter)}
              </tr>
            );
          }
        })}
      </>
    );
  };

  let handleSeatSelection = (e, seatData, seatIndex) => {
    e.target.classList.toggle("active");
    let seatDataWithIndex = { ...seatData, seatIndex };
    dispatch({ type: SET_SELECTING_SEAT, data: seatDataWithIndex });
    // dispatch({type:SET_SELECTING_SEAT,data:})
  };

  return (
    <div className="seatSelect ">
      <div
        style={{ maxWidth: 1200 }}
        className="w-full md:w-11/12 h-full mx-auto"
      >
        {/* Movie Info */}
        <div
          style={{
            backgroundImage: `url(${thongTinPhim.hinhAnh})`,
            backgroundSize: "cover",
            backgroundRepeat: "no-repeat",
            backgroundPosition: "left",
            backgroundBlendMode: "multiply",
            backgroundColor: "#fafcff",
          }}
          className=" text-center h-28 relative w-full mx-auto rounded my-2 px-2"
        >
          <div
            style={{
              backgroundColor: "rgba(245, 245, 245, 0.904)",
              filter: "blur(70px)",
            }}
            className="h-full w-full"
          ></div>
          <div className="absolute top-0 left-0 w-full items-center h-full flex flex-col justify-center">
            <p className="text-center text-lg text-blue-900 font-bold mb-0">
              {thongTinPhim.tenPhim}
            </p>
            <p className="mb-0 mt-2 font-bold">
              <span className="capitalize">
                {moment(new Date(thongTinPhim.ngayChieu)).format(
                  "dddd,[Ngày ]DD[/]MM"
                )}{" "}
                -
              </span>
              <span className="ml-2 px-2 text-white py-1 bg-blue-800 rounded">
                {moment(thongTinPhim.gioChieu, "hh:mm").format("hh[:]mm")}
              </span>
              ~
              <span className="text-white py-1 bg-blue-700 px-2 rounded">
                {moment(thongTinPhim.gioChieu, "hh:mm")
                  .add(2, "hours")
                  .format("hh[:]mm ")}
              </span>
            </p>
          </div>
        </div>
        {/* Screen Img */}
        <div className="screenImg w-full relative">
          <img className="w-full" src={screenImg} alt="" />
        </div>
        {/* Seat Map */}
        <div className="overflow-auto">
          <table className="w-full">
            <tbody>
              <tr>
                <th className=""></th>

                <th>1</th>
                <th>2</th>
                <th>3</th>
                <th>4</th>
                <th>5</th>
                <th>6</th>
                <th>7</th>
                <th>8</th>
                <th>9</th>
                <th>10</th>
                <th>11</th>
                <th>12</th>
                <th>13</th>
                <th>14</th>
                <th>15</th>
                <th>16</th>
              </tr>
              {renderSeatRow()}
            </tbody>
          </table>
          <div className="seatsGuide flex justify-center items-end text-gray-500">
            <div
              className="seatType text-center"
              style={{ width: "5.9%", minWidth: "84px" }}
            >
              <img src={chairImg} alt="" />
              <p>Ghế thường</p>
            </div>
            <div
              className="seatType text-center"
              style={{ width: "5.9%", minWidth: "84px" }}
            >
              <img src={vipChairImg} alt="" />
              <p>Ghế VIP</p>
            </div>
            <div
              className="seatType text-center"
              style={{ width: "5.9%", minWidth: "110px" }}
            >
              <img src={chairImg} alt="" />
              <p>Ghế đang chọn</p>
            </div>
            <div
              className="seatType text-center"
              style={{ width: "5.9%", minWidth: "110px" }}
            >
              <img src={chairImg} alt="" />
              <p>Đã có người đặt</p>
            </div>
          </div>
        </div>
        {/* Selected Seats  */}
        <div
          style={{ marginLeft: "3%", marginRight: "3%" }}
          className="my-3 text-lg md:text-xl p-2  rounded font-semibold border  border-gray-300"
        >
          <span className="mr-2 text-blue-500 ">Ghế đã chọn:</span>
          {selectedSeats.map((item, index) => {
            return (
              <span
                className="selectedSeatIndex"
                key={"seatIndex" + index + index}
              >
                {index == 0 ? item.seatIndex : `, ${item.seatIndex}`}
              </span>
            );
          })}
        </div>
        {/* Total  */}
        <div
          style={{ marginLeft: "3%", marginRight: "3%" }}
          className=" my-3 text-lg md:text-xl p-2  rounded font-semibold border  border-gray-300"
        >
          <span className="mr-2 text-blue-500">Tổng tiền:</span>
          <span className="text-orange-500">
            {subtotal.toLocaleString("vn")}
            <span>&#8363;</span>
          </span>
        </div>
        {/* Control Section */}
        <div className="mt-5 w-full px-4 md:px-0 grid grid-cols-1   md:grid-cols-2 text-white text-base font-bold">
          <button
            onClick={() => {
              handleChangeView("bookingPayment");
            }}
            className="mx-auto w-full md:w-52 mt-4 md:mt-0 px-5 py-3 bg-blue-400 rounded order-last md:order-none"
          >
            Về trang chi tiết phim
          </button>
          <button
            onClick={() => {
              handleChangeView("bookingPayment");
            }}
            className="mx-auto w-full md:w-52  px-5 py-3 bg-blue-400 rounded"
          >
            Thanh toán
          </button>
        </div>
      </div>
    </div>
  );
}
