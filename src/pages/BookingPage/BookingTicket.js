import React from "react";
import { useEffect } from "react";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import { getTheaterInfo } from "~/redux/actions/TicketAction";
import { localServ } from "~/services/localService";
import BookingPayment from "./BookingPayment";
import BookingResult from "./BookingResult";
import BookingHeader from "./BookingHeader";
import SeatSelectSection from "./SeatSelectSection";
import theaterImg from "~/assets/images/theater_bg.jpg";

export default function BookingTicket() {
  let userInfo = localServ.user.get();
  let params = useParams();
  let dispatch = useDispatch();
  let handleChangeView = (view) => {
    let renderView = viewsArr.find((item) => item.view === view);
    setCurrentView(renderView);
  };
  useEffect(() => {
    dispatch(getTheaterInfo(params.id));
  }, []);
  let viewsArr = [
    {
      id: 1,
      view: "seatSelect",
      element: <SeatSelectSection handleChangeView={handleChangeView} />,
    },
    {
      id: 2,
      view: "bookingPayment",
      element: <BookingPayment handleChangeView={handleChangeView} />,
    },
    {
      id: 3,
      view: "bookingResult",
      element: <BookingResult handleChangeView={handleChangeView} />,
    },
  ];
  let [currentView, setCurrentView] = useState(viewsArr[0]);
  let headerHeight = "60px";
  return (
    <div className="">
      {/* Header */}
      <div style={{ height: headerHeight }} className="">
        <div className="fixed top-0 left-0 right-0 z-10">
          <BookingHeader
            headerHeight={headerHeight}
            currentViewId={currentView.id}
            handleChangeView={handleChangeView}
            userInfo={userInfo}
          />
        </div>
      </div>
      {/* Content */}
      <div
        style={{
          backgroundImage: `url(${theaterImg})`,
          backgroundSize: "cover",
          backgroundPosition: "center",
          height: `calc(100vh - ${headerHeight})`,
        }}
        className=""
      >
        <div
          style={{ height: `calc(100vh - ${headerHeight})` }}
          className=" container mx-auto bg-white"
        >
          {currentView.element}
        </div>
      </div>
    </div>
  );
}
