import React from "react";

export default function BookingPayment({ handleChangeView }) {
  return (
    <div>
      <button
        onClick={() => {
          handleChangeView("seatSelect");
        }}
        className="px-5 bg-blue-300 py-2 rounded"
      >
        BackToSeatSelecting
      </button>
    </div>
  );
}
