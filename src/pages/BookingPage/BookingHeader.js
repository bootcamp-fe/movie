import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faHouse } from "@fortawesome/free-solid-svg-icons";
import "./BookingHeader.scss";
import UserMenu from "~/layouts/HomeLayout/components/UserMenu";
import { useSelector } from "react-redux";

export default function BookingHeader({
  handleChangeView,
  currentViewId,
  headerHeight,
}) {
  let theaterInfo = useSelector((state) => state.TicketReducer.theaterInfo);
  let { thongTinPhim = "" } = theaterInfo;
  console.log(theaterInfo);
  return (
    <div
      style={{ height: headerHeight }}
      className="headerContainer flex items-center"
    >
      <div className="container px-2 mx-auto flex justify-between items-center">
        <a className="text-3xl" href="/">
          <FontAwesomeIcon icon={faHouse} />
        </a>
        <div className="text-right">
          <p className="mb-0">{thongTinPhim.tenCumRap}</p>
          <p className="mb-0 text-gray-600 text-xs md:text-md">
            {thongTinPhim.diaChi}
          </p>
        </div>
      </div>
    </div>
  );
}
